## Siete y Medio Card Game

This is a repository of files required to play the game "siete y medio"
It is a game that is very similar to blackjack

**Main Goals**

* Learn how to use git and version control
* Program a working version of the game
* Use Bitbucket as the location of the remote repository

**Implementation**

* We will first create a header file, called Cards.h, that will contain all of the information about our classes Cards, Hand, and Player. This includes member variable and function declarations.
* Then we will create another file, Cards.cpp, which will implement the header file stated above.
* During implementation, I created another branch, "furtherimplementation," which did some experimentation with the Hand and Player classes. I then merged this branch with the master branch once I was finished.
* During the process, I also made a simple error meant to be caught. I created a separate Bitbucket account, forked the repository to my new account, patched the error, and approved and merged the final code for Cards.cpp to my initial account.



