#include "cards.h"
#include <cstdlib>
#include <iostream>
#include <iomanip>



/* *************************************************
 Card class
 ************************************************* */


Card::Card(){
    int r = 1 + rand() % 4;
    switch (r) {
        case 1: suit = OROS; break;
        case 2: suit = COPAS; break;
        case 3: suit = ESPADAS; break;
        case 4: suit = BASTOS; break;
        default: break;
    }
    
    r = 1 + rand() % 10;
    switch (r) {
        case  1: rank = AS; break;
        case  2: rank = DOS; break;
        case  3: rank = TRES; break;
        case  4: rank = CUATRO; break;
        case  5: rank = CINCO; break;
        case  6: rank = SEIS; break;
        case  7: rank = SIETE; break;
        case  8: rank = SOTA; break;
        case  9: rank = CABALLO; break;
        case 10: rank = REY; break;
        default: break;
    }
}

// Accessor: returns a string with the suit of the card in Spanish
string Card::get_spanish_suit() const {
    string suitName;
    switch (suit) {
        case OROS:
            suitName = "oros";
            break;
        case COPAS:
            suitName = "copas";
            break;
        case ESPADAS:
            suitName = "espadas";
            break;
        case BASTOS:
            suitName = "bastos";
            break;
        default: break;
    }
    return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish
string Card::get_spanish_rank() const {
    string rankName;
    switch (rank) {
        case AS:
            rankName = "As";
            break;
        case DOS:
            rankName = "Dos";
            break;
        case TRES:
            rankName = "Tres";
            break;
        case CUATRO:
            rankName = "Cuatro";
            break;
        case CINCO:
            rankName = "Cinco";
            break;
        case SEIS:
            rankName = "Seis";
            break;
        case SIETE:
            rankName = "Siete";
            break;
        case SOTA:
            rankName = "Sota";
            break;
        case CABALLO:
            rankName = "Caballo";
            break;
        case REY:
            rankName = "Rey";
            break;
        default: break;
    }
    return rankName;
}



// Accessor: returns a string with the suit of the card in English
string Card::get_english_suit() const {
    string suitName;
    switch (suit) {
        case OROS:
            suitName = "Golds";
            break;
        case COPAS:
            suitName = "Cups";
            break;
        case ESPADAS:
            suitName = "Swords";
            break;
        case BASTOS:
            suitName = "Clubs";
            break;
            
        default: break;
    }
    return suitName;
}

// Accessor: returns a string with the rank of the card in English
string Card::get_english_rank() const {
    string rankName;
    switch (rank) {
        case AS:
            rankName = "One";
            break;
        case DOS:
            rankName = "Two";
            break;
        case TRES:
            rankName = "Three";
            break;
        case CUATRO:
            rankName = "Four";
            break;
        case CINCO:
            rankName = "Five";
            break;
        case SEIS:
            rankName = "Six";
            break;
        case SIETE:
            rankName = "Seven";
            break;
        case SOTA:
            rankName = "Jack";
            break;
        case CABALLO:
            rankName = "Knight";
            break;
        case REY:
            rankName = "King";
            break;
        default: break;
    }
    return rankName;
}


// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
    return static_cast<int>(rank) + 1 ;
}

// Comparison operator for cards
// Returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
    return rank < card2.rank;
}

/* *************************************************
 Hand class
 ************************************************* */
//Default constructor of a hand containing an initial card
    Hand::Hand() {
     hand.push_back(initial);
}
void Hand::draw_Card() {
    Card deal = Card();
    hand.push_back(deal);
    
    return;
}

void Hand::reset_Hand() {
    int n = hand.size();
    for (int i=0; i<n; i++)
        hand.pop_back();
    initial = Card();
    hand.push_back(initial);
    
    return;
}

vector<Card> Hand::get_Hand() {
    return hand;
}

void Hand::print() {
    // the longest string is "Caballo of Espadas" which is 18 characters long
    const int M = 18;
    int n = hand.size();
    for (int i=0;i<n;i++) {
        string spanish = hand[i].get_spanish_rank() + " of " + hand[i].get_spanish_suit();
        string english = hand[i].get_english_rank() + " of " + hand[i].get_english_suit();
        cout << "\t" << spanish;
        cout << setw((M-spanish.length()) + 3) << "(" << english << ").";
        if (i<n-1)
            cout << endl;
    }
    return;
}

void Hand::print_new() {
    int n = hand.size();
    int new_card = n-1;
    string spanish = hand[new_card].get_spanish_rank() + " of " + hand[new_card].get_spanish_suit();
    string english = hand[new_card].get_english_rank() + " of " + hand[new_card].get_english_suit();
    cout << "\t" << spanish << " (" << english << ")." << endl << endl;
    return;
}

double Hand::sum() {
    total = 0;
    int n = hand.size();
    for (int i=0;i<n;i++) {
        if (hand[i].get_english_rank()== "Jack" || hand[i].get_english_rank()== "Knight" || hand[i].get_english_rank()== "King")
            total+=0.5;
        else total+=hand[i].get_rank();
    }
    
    return total;
}

void Hand::print_to_file(int a, int bet, int remainingMoney, Hand &deal) {
    ofstream fout;
    
    fout.open("Gamelog.txt",ios::app);
    for(int i=0;i<45;i++)
        fout << "-";
    fout << "\nGame number: " << a << "\t" << "Money Left: $" << remainingMoney << "\nBet: " << bet << endl << endl;
    fout << "Your cards: \n";
    // the longest string is "Caballo of Espadas" which is 18 characters long
    const int M = 18;
    int n = hand.size();
    for (int i=0;i<n;i++) {
        string spanish = hand[i].get_spanish_rank() + " of " + hand[i].get_spanish_suit();
        string english = hand[i].get_english_rank() + " of " + hand[i].get_english_suit();
        fout << "\t" << spanish;
        fout << setw((M-spanish.length()) + 3) << "(" << english << ")." << endl;
    }
    
    fout << "Your total: " << sum() << endl << endl;
    fout << "Dealer's Cards: \n";
    int m = deal.hand.size();
    for (int i=0;i<m;i++) {
        string spanish = deal.hand[i].get_spanish_rank() + " of " + deal.hand[i].get_spanish_suit();
        string english = deal.hand[i].get_english_rank() + " of " + deal.hand[i].get_english_suit();
        fout << "\t" << spanish;
        fout << setw((M-spanish.length()) + 3) << "(" << english << ").";
        fout << endl;
    }
    fout << "Dealer's total is " << deal.sum() << endl;
    fout.close();
    return;
}

/* *************************************************
 Player class
 ************************************************* */
// Implement the member functions of the Player class here.

Player::Player(int m) {
    money = m;
}

void Player::player_loses(int l) {
    money-=l;
    return;
}

void Player::player_wins(int w) {
    money += w;
    return;
}

int Player::get_money() {
    return money;
}







