#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <iomanip>
#include "cards.h"
using namespace std;

// Global constant
const int starting_money = 100;

int main(){
    srand(static_cast<int>(time(0)));
    
    Player one = Player(starting_money);
    Hand dealer = Hand();
    Hand player = Hand();
    string response = "yes";
    int bet = 0;
    int gameNumber = 0;
    while (one.get_money()>0 && one.get_money()<1000) {
        
        cout << "You have " << one.get_money() << " dollars. Enter bet: ";
        cin >> bet;
        response = "yes";
        while ((response == "yes" || response == "y" || response == "Yes")&& player.sum()<=7.5) {
        cout << "Your Cards: \n";
        player.print();
        cout << endl << "Your total is " << player.sum() << ". Do you want another card(y/n)? ";
        cin >> response;
            if (response == "yes" || response == "y" || response == "Yes") {
                player.draw_Card();
                cout << "\nNew Card: \n";
                player.print_new();
                
            }
            if(player.sum()>7.5) {
            cout << "Your Cards: \n";
            player.print();
            cout << endl << "Your total is " << player.sum() << ".";
                cout << endl;
            }
        }
        
        cout << "Dealer's Cards:";
        dealer.print();
        cout << endl << "The dealer's total is " << dealer.sum() << ".\n";
        
    while (dealer.sum() < 5.5 ) {
        dealer.draw_Card();
        cout << "\nNew Card: \n";
        dealer.print_new();
        cout << "Dealer's Cards: \n";
        dealer.print();
        cout << endl << "The dealer's total is " << dealer.sum() << endl;
    }
    gameNumber++;
        player.print_to_file(gameNumber,bet,one.get_money(), dealer);
    if (player.sum()>7.5) {
        one.player_loses(bet);
        cout << "Too bad. You Lose " << bet << "." << endl;
        
    }
    else if (player.sum()<=7.5 && dealer.sum()<player.sum()) {
        one.player_wins(bet);
        cout << "You win " << bet << "." << endl;
    }
    else if (player.sum()<=7.5 && dealer.sum()>7.5) {
        one.player_wins(bet);
        cout << "You win " << bet << "." << endl;
    }
    else if (player.sum()==dealer.sum())
        cout << endl << "Nobody wins." << endl;
    else {
        one.player_loses(bet);
        cout << "Too bad. You lose " << bet << "." << endl;
    }
    if (one.get_money()==0)
            cout << "You have " << 0 << " dollars. GAME OVER \nCome back when you have more money.\nBye!\n";
    if (one.get_money()>=1000)
            cout << "\nCongratulations. You beat the Casino!\n\nBye!";
        
        
        player.reset_Hand();
        dealer.reset_Hand();
    }

    return 0;
}




